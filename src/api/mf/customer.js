import request from '@/utils/request'

// 查询客户主表列表
export function listCustomer(query) {
  return request({
    url: '/mf/customer/list',
    method: 'get',
    params: query
  })
}

// 查询客户主表详细
export function getCustomer(customerId) {
  return request({
    url: '/mf/customer/' + customerId,
    method: 'get'
  })
}

// 新增客户主表
export function addCustomer(data) {
  return request({
    url: '/mf/customer',
    method: 'post',
    data: data
  })
}

// 修改客户主表
export function updateCustomer(data) {
  return request({
    url: '/mf/customer',
    method: 'put',
    data: data
  })
}

// 删除客户主表
export function delCustomer(customerId) {
  return request({
    url: '/mf/customer/' + customerId,
    method: 'delete'
  })
}
