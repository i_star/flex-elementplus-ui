import { defineStore } from 'pinia';
import {useStorage} from "@vueuse/core";

const sidebarStatus = useStorage('sidebarStatus', '1');
const size = useStorage('size', 'default');
const useAppStore = defineStore(
  'app',
  {
    state: () => ({
      sidebar: {
        opened: sidebarStatus.value ? !!+sidebarStatus.value : true,
        withoutAnimation: false,
        hide: false
      },
      device: 'desktop',
      size: useStorage('size', 'default')
    }),
    actions: {
      toggleSideBar(withoutAnimation) {
        if (this.sidebar.hide) {
          return false;
        }
        this.sidebar.opened = !this.sidebar.opened
        this.sidebar.withoutAnimation = withoutAnimation
        if (this.sidebar.opened) {
          sidebarStatus.value = '1';
        } else {
          sidebarStatus.value = '0';
        }
      },
      closeSideBar(withoutAnimation) {
        sidebarStatus.value = '0';
        this.sidebar.opened = false
        this.sidebar.withoutAnimation = withoutAnimation
      },
      toggleDevice(device) {
        this.device = device
      },
      setSize(size) {
        this.size = size;
      },
      toggleSideBarHide(status) {
        this.sidebar.hide = status
      }
    }
  })

export default useAppStore
